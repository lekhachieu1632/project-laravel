export const METHOD_GET = "GET";
export const METHOD_POST = "POST";
export const METHOD_PUT= "PUT";
export const METHOD_DELETE = "DELETE";

function setupajax()
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
}

const base = {
    callApiWithFormData: function callApiWithFormData( url, method, data){
        setupajax();
        return $.ajax({
            url: url,
            type: method,
            data: data,
            contentType: false,
            processData: false,
        })
    },

    callApi: function callApi(url, method = METHOD_GET, data= null){
        setupajax();
        return $.ajax({
            url: url,
            type: method,
            data: data
        })
    },

    confirm: function (){
        return swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this imaginary file!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
    },

    showMessage: function showMessage(status, title)
    {
        Swal.fire({
            position: 'top-end',
            icon: status,
            title: title,
            showConfirmButton: false,
            timer: 1500
        })
    },
    
    showError: function (response){
        $.each(response.responseJSON.errors, function (name, message){
            $('#error-' + name).text(message[0])
            $('#modal-product').on('hide.bs.modal', function (){
                $('#error-' + name).text("");
            })
        })
    },
};

export default base;
