import base, {METHOD_GET, METHOD_POST, METHOD_PUT, METHOD_DELETE} from "./base.js";

(function ($, window, document){
    $(function (){

        //create
        $('#add-product').on('click', function (){
            let formData = new FormData($('#form-create')[0]);
            let url = $(this).data('url');

            base.callApiWithFormData(url, METHOD_POST, formData)
                .done(function (response){
                    base.showMessage(response.message);
                    $('#modal-product').modal('hide');
                    $('#form-create').find('input').val("");
                })
                .error(function (response){
                    base.showError(response);
                })
        });

        //update
        $('#update-product').on('click', function (){
            let formData = new FormData($('#form-detail')[0]);
            formData.append('_method', METHOD_PUT);
            let url = $(this).data('url');

            base.callApiWithFormData(url, METHOD_POST, formData)
               .done(function (response){
                   base.showMessage(response.message);
                   $('#modal-product-edit').modal('hide');
                   $('#form-detail').find('input').val("");
               })
               .error(function (response){
                   base.showError(response);
               });
        });

        //delete
        $('#delete-product').on('click', function (){
            let url = $(this).data('url');
            base.confirm()
               .then(value => {
                    if(value){
                        base.callApi(url, METHOD_DELETE)
                            .done(function (response){
                                base.showMessage(response.message)
                            });
                    }
               });
        });
    });
}(window.jQuery, window, document))
