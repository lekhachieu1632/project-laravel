@extends('layouts.app')

@section('content')
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card my-4">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3 row">
                            <h4 class="text-white  ps-3 col-6">USER/CREATE</h4>


                        </div>
                    </div>
                    <div class="card-body px-0 pb-2">
                        <div class="table-responsive p-0">
                            <div class="">
                                <div class="card card-plain">
                                    <div class="card-body">
                                        <form role="form" method="POST" action="{{route('users.store')}}">
                                            @csrf

                                            <div class="input-group input-group-outline mb-3">
                                                <input type="text" class="form-control" name="name" placeholder="Name ...">
                                                @error('name')
                                                <span class="alert-danger" role="alert">
                                                    {{$message}}
                                                </span>
                                                @enderror
                                            </div>

                                            <div class="input-group input-group-outline mb-3">
                                                <input type="email" class="form-control" name="email" placeholder="Email ...">
                                                @error('email')
                                                <span class="alert-danger" role="alert">
                                                    {{$message}}
                                                </span>
                                                @enderror
                                            </div>

                                            <div class="input-group input-group-outline mb-3">
                                                <input type="password" class="form-control" name="password" placeholder="Password">
                                                @error('password')
                                                <span class="alert-danger" role="alert">
                                                    {{$message}}
                                                </span>
                                                @enderror
                                            </div>

                                            <div class="input-group input-group-outline mb-3">
                                                <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password">
                                                @error('password')
                                                <span class="alert-danger" role="alert">
                                                    {{$message}}
                                                </span>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <label class="text-dark">
                                                    <h6 class="mb-0">Role</h6>
                                                </label>
                                                <div class="row row-cols-3 pl-3 pr-3">
                                                    @foreach($rolesWithoutSuperAdmin as $role)
                                                        <div class="form-check col-4">
                                                            <label class="form-check-label text-dark font-weight-bold">
                                                                <input class="form-check-input" type="checkbox"
                                                                       name="role_id[]"
                                                                       value="{{$role->id}}">
                                                                        {{$role->display_name}}
                                                                <span class="form-check-sign">
                                                                    <span class="check"></span>
                                                                 </span>
                                                            </label>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                            @hasPermission('user-store')
                                                <div class="text-center">
                                                    <button type="submit"
                                                            class="btn btn-lg bg-gradient-primary btn-lg w-100 mt-4 mb-0">
                                                        Create
                                                    </button>
                                                </div>
                                            @else
                                            @endhasPermission
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
