@extends('layouts.app')

@section('content')
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card my-4">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3 row">
                            <h4 class="text-white  ps-3 col-6">USER/SHOW</h4>


                        </div>
                    </div>
                    <div class="card-body px-0 pb-2">
                        <div class="table-responsive p-0">
                            <table class="table align-items-center mb-0">
                                <thead>
                                <tr>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder">Id</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Name</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Email</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Role</th>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        <p class="text-center text-xs text-secondary mb-0">{{ $user->id }}</p>
                                    </td>

                                    <td>
                                        <p class="text-xs text-secondary mb-0">{{ $user->name }}</p>
                                    </td>

                                    <td>
                                        <p class="text-xs text-secondary mb-0">{{ $user->email }}</p>
                                    </td>

                                    <td>
                                        @foreach($user->roles as $role)
                                            <p class="text-xs mb-0 btn btn-outline-success btn-sm">{{$role->display_name}}</p>
                                        @endforeach
                                    </td>

                                    <td class="align-middle text-center text-sm">
                                        <a href="javascript:;" class="text-xs mb-0 btn btn-warning btn-sm" data-toggle="tooltip" data-original-title="Edit user">
                                            Edit
                                        </a>
                                        <a href="javascript:;" class="text-xs mb-0 btn btn-danger btn-sm" data-toggle="tooltip" data-original-title="Delete user">
                                            Delete
                                        </a>
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
