@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'products'
])
@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow">
                    <div class="card-header">
                        <h2 class="mb-0 text-dark text-center">
                            Edit Product
                        </h2>
                    </div>
                    <div class="card-body">
                        <form enctype="multipart/form-data" action="{{ route('products.update', $product->id) }}" method="POST">
                            @csrf
                            @method("PUT")
                            <div class="input-group input-group-outline mb-3">

                                <input type="text" class="form-control" name="name"
                                       value="{{ $product->name }}" placeholder="Name product ...">
                                @error('name')
                                <span class="alert-danger" role="alert">{{$message}}</span>
                                @enderror
                            </div>

                            <div class="input-group input-group-outline mb-3">
                                <input type="text" class="form-control" name="price"
                                       value="{{ $product->price }}" placeholder="Price ...">
                                @error('price')
                                <span class="alert-danger" role="alert">{{$message}}</span>
                                @enderror
                            </div>


                            <div class="form-group">
                                <label class="text-dark">
                                    <h6 class="m-0">Category</h6>
                                </label>
                                <select class="form-control form-control-lg rounded" name="category_id[]" required>
                                    <option value="">--Select Category--</option>
                                    @foreach($categories as $category)
                                        <option @foreach($product->categories as $category_item)
                                                @if($category->id == $category_item->id)
                                                selected
                                                @endif
                                                @endforeach
                                                class="text-dark" value="{{ $category->id }}">
                                            {{ $category->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>


                            <div class="input-group input-group-outline mb-3">
                                <input type="text" class="form-control" name="description"
                                       value="{{ $product->description }}" placeholder="Description ...">
                                @error('description')
                                <span class="alert-danger" role="alert">{{$message}}</span>
                                @enderror
                            </div>


                            <div class="form-group">
                                <div class="form-group">
                                    <label class="text-dark">
                                        <h6 class="m-0">Image</h6>
                                    </label>
                                    <input type="file" class="form-control" id="image" name="image" value="{{ $product->image }}">
                                    Choose file
                                </div>
                                <div class="form-group">
                                    <div style="width: 10rem; height: 10rem">
                                        <img id="showImage" alt="" style="width: 10rem; height: 10rem">
                                    </div>
                                </div>
                            </div>
                            @hasPermission('products-store')
                               <button type="submit"
                                        class="btn btn-lg bg-gradient-primary btn-lg w-100 mt-4 mb-0">Edit
                               </button>
                            @else
                            @endhasPermission
                        </form>
                    </div>
                    <div class="card-footer">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#image').change(function (e) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#showImage').attr('src', e.target.result);
                }
                reader.readAsDataURL(e.target.files['0']);
            })
        })
    </script>
@endpush
