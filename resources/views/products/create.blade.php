@extends('layouts.app')

@section('content')
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card my-4">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3 row">
                            <h4 class="text-white  ps-3 col-6">PRODUCT/CREATE</h4>
                        </div>
                    </div>
                    <div class="card-body px-0 pb-2">
                        <div class="table-responsive p-0">

                            <div class="card card-plain">
                                <div class="card-body">
                                    <form id="form-create-product">

                                        <div class="input-group input-group-outline mb-3">
                                            <input type="text" class="form-control" name="name"
                                                   placeholder="Name product ...">
                                            @error('name')
                                            <span class="alert-danger" role="alert">{{$message}}</span>
                                            @enderror
                                        </div>

                                        <div class="input-group input-group-outline mb-3">
                                            <input type="text" class="form-control" name="price"
                                                   placeholder="Price ...">
                                            @error('price')
                                            <span class="alert-danger" role="alert">{{$message}}</span>
                                            @enderror
                                        </div>


                                        <div class="form-group">
                                            <label class="text-dark">
                                                <h6 class="m-0">Category</h6>
                                            </label>
                                            <select class="form-control form-control-lg rounded" name="category_ids[]" required>
                                                <option value="">--Select Category--</option>
                                                @foreach($categories as $category)
                                                    <option class="text-dark" value="{{ $category->id }}">
                                                        {{ $category->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>


                                        <div class="input-group input-group-outline mb-3">
                                            <input type="text" class="form-control" name="description"
                                                   placeholder="Description ...">
                                            @error('description')
                                            <span class="alert-danger" role="alert">{{$message}}</span>
                                            @enderror
                                        </div>


                                        <div class="form-group">
                                            <div class="form-group">
                                                <label class="text-dark">
                                                    <h6 class="m-0">Image</h6>
                                                </label>
                                                <input type="file" class="form-control" id="image" name="image">
                                                Choose file
                                            </div>
                                            <div class="form-group">
                                                <div style="width: 10rem; height: 10rem">
                                                    <img id="showImage" alt="" style="width: 10rem; height: 10rem">
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                    @hasPermission('products-update')
                                    <button id="btn-create-product" data-url="{{ route('products.store') }}"
                                            class="btn btn-lg bg-gradient-primary btn-lg w-100 mt-4 mb-0">Create
                                    </button>
                                    @else
                                        @endhasPermission
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#image').change(function (e) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#showImage').attr('src', e.target.result);
                }
                reader.readAsDataURL(e.target.files['0']);
            })
        })
    </script>

    <script type="module" src="{{asset('assets/js/product.js')}}"></script>
@endpush


