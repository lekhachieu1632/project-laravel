@extends('layouts.app')

@section('content')
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card my-4">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3 row">
                            <h4 class="text-white  ps-3 col-6">PRODUCT</h4>

                            <form method="GET" class="col-4">
                                <div class="input-group input-group-sm m-0 border border-white border-3">
                                    <input type="text" name="search"
                                           class="form-control ps-3 text-white "
                                           placeholder="Search...">
                                    <button class="btn btn-sm m-0 " type="submit">
                                        <div class="input-group-append opacity-10 text-white">
                                            <i style="font-size: 12px" class="fas fa-search"></i>

                                        </div>
                                    </button>
                                </div>
                            </form>

                            <div class="col-1 text-right">
                                @hasPermission('product-create')
                                    <button type="button" class="btn btn-sm m-0 border border-white border-3 text-white" data-toggle="modal" data-target="#modal-product">
                                        <i style="font-size: 12px" class="fas fa-plus fa-2x"></i>
                                    </button>
                                    @else
                                @endhasPermission
                            </div>
                        </div>
                    </div>
                    <div class="card-body px-0 pb-2">
                        <div class="table-responsive p-0">
                            <table class="table align-items-center mb-0">
                                <thead>
                                <tr>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder">
                                        Id
                                    </th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Name</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Price</th>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder">Category</th>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder">Image</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Description</th>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder">
                                        Action
                                    </th>
                                </tr>
                                </thead>
                                    <tbody>
                                    @foreach($products as $product)
                                        <tr>
                                            <td>
                                                <p class="text-center text-xs text-secondary mb-0">{{ $product->id }}</p>
                                            </td>

                                            <td>
                                                <p class="text-xs text-secondary mb-0">{{ $product->name }}</p>
                                            </td>

                                            <td>
                                                <p class="text-xs text-secondary mb-0">${{ number_format($product->price, 0, '', '.') }}</p>
                                            </td>

                                            <td>
                                                @foreach($product->categories as $category)
                                                    <p class="text-center text-xs text-secondary mb-0">
                                                        {{ $category->name }}
                                                    </p>
                                                @endforeach
                                            </td>

                                            <td>
                                                <div class="logo-mini text-center">
                                                    <img src="{{ asset('assets/img/'.$product->image) }}"
                                                         alt="{{ $product->image }}"
                                                         style="height: 10rem; width: 10rem">
                                                </div>
                                            </td>

                                            <td>
                                                <p class="text-xs text-secondary mb-0 ">{{ $product->description }}</p>
                                            </td>



                                            <td class="align-middle text-center text-sm">
                                                <div class="container">
                                                    <div class="d-flex justify-content-center">
                                                        @hasPermission('product-show')
                                                            <a href="{{ route('products.show', $product->id) }}"
                                                               class="text-xs mb-0 btn btn-success btn-sm m-1" data-toggle="tooltip"
                                                               data-original-title="Show user">
                                                                Show
                                                            </a>
                                                        @else
                                                        @endhasPermission

                                                        @hasPermission('product-destroy')
                                                            <button id="delete-product" class="text-xs mb-0 btn btn-danger btn-sm m-1" data-url="{{ route('products.destroy', $product->id) }}">Delete
                                                            </button>
                                                        @else
                                                        @endhasPermission

                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                            </table>
                            <br>
                            <div class="container justify-content-center">
                                {{$products->appends(request()->all())->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--  Modal form add  --}}
    <div class="modal fade" id="modal-product" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel" >Add Product</h4>
                </div>
                <div class="modal-body">
                    <form id="form-create">
                        <div class="form-group">
                            <label for="name-product">Name</label>
                            <input type="text" class="form-control" id="name-product"  placeholder="Name..." name="name">
                            <small id="error-name" class="form-text text-muted" style="color: red !important;"></small>
                        </div>
                        <div class="form-group">
                            <label for="price-product">Price</label>
                            <input type="text" class="form-control" id="price-product" placeholder="Price..." name="price">
                            <small id="error-price" class="form-text text-muted" style="color: red !important;"></small>
                        </div>
                        <div class="form-group">
                            <label for="categories-product">Category</label>
                            <select class="form-control form-control-lg rounded" name="category_ids[]" required>
                                <option value="">--Select Category--</option>
                                @foreach($categories as $category)
                                    <option class="text-dark" value="{{ $category->id }}">
                                        {{ $category->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="description-product">Description</label>
                                    <textarea type="text" class="form-control" id="description-product" placeholder="Description..." style="max-width: 100%; height: 150px" name="description"></textarea>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Thumbnail</label>
                                <div class="thumbnail-product">
                                    <div class="box-thumbnail">
{{--                                            <img src="{{asset('images/img-default.png')}}" alt="" width="100%" height="100%" id="thumbnail">--}}
                                    </div>
                                    <div class="btn btn-sm btn-success btn-add-thumbnail">
                                        Add image
                                        <input type="file" id="file-thumbnail" name="image">
                                    </div>
                                    <small id="error-image" class="form-text text-muted" style="color: red !important;"></small>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="add-product" data-url="{{ route('products.store') }}">Create</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button>
                </div>
            </div>
        </div>
    </div>
    {{--  End modal form add  --}}


@endsection

@push('scripts')
    <script type="module" src="{{asset('assets/js/product.js')}}"></script>
@endpush
