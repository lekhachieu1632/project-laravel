@extends('layouts.app')

@section('content')
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card my-4">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3 row">
                            <h4 class="text-white  ps-3 col-6">CATEGORY/CREATE</h4>
                        </div>
                    </div>
                    <div class="card-body px-0 pb-2">
                        <div class="table-responsive p-0">

                            <div class="card card-plain">
                                <div class="card-body">
                                    <form role="form" method="POST" action="{{ route('categories.store') }}">
                                        @csrf
                                        <div class="input-group input-group-outline mb-3">

                                            <input type="text" class="form-control" name="name"
                                                   placeholder="Name Category ...">
                                            @error('name')
                                            <span class="alert-danger" role="alert">{{$message}}</span>
                                            @enderror
                                        </div>

                                        <div class="input-group input-group-outline mb-3">
                                            <input type="text" class="form-control" name="parent_id"
                                                   placeholder="Parent ID ...">
                                            @error('parent_id')
                                            <span class="alert-danger" role="alert">{{$message}}</span>
                                            @enderror
                                        </div>

                                        @hasPermission('categories-store')
                                        <button type="submit"
                                                class="btn btn-lg bg-gradient-primary btn-lg w-100 mt-4 mb-0">Create
                                        </button>
                                        @else
                                            @endhasPermission
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
