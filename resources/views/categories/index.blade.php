@extends('layouts.app')

@section('content')
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card my-4">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3 row">
                            <h4 class="text-white  ps-3 col-6">CATEGORY</h4>

                            <form method="GET" class="col-4">
                                <div class="input-group input-group-sm m-0 border border-white border-3">
                                    <input type="text" name="search"
                                           class="form-control ps-3 text-white "
                                           placeholder="Search...">
                                    <button class="btn btn-sm m-0 " type="submit">
                                        <div class="input-group-append opacity-10 text-white">
                                            <i style="font-size: 12px" class="fas fa-search"></i>

                                        </div>
                                    </button>
                                </div>
                            </form>

                            <div class="col-1 text-right">
                                @hasPermission('category-create')
                                    <a href="{{ route('categories.create') }}"
                                       class="btn btn-sm m-0 border border-white border-3 text-white">
                                        <i style="font-size: 12px" class="fas fa-plus fa-2x"></i>
                                    </a>
                                @else
                                @endhasPermission
                            </div>


                        </div>
                    </div>
                    <div class="card-body px-0 pb-2">
                        <div class="table-responsive p-0">
                            <table class="table align-items-center mb-0">
                                <thead>
                                <tr>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder">Number</th>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder">Parent Name</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Name</th>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <p style="display: none;">{{ $stt = 0 }}</p>
                                @foreach($categories as $category)
                                    <tr>
                                        <td>
                                            <p class="text-center text-xs text-secondary mb-0">{{ $stt = $stt + 1 }}</p>
                                        </td>

                                        <td>
                                            <p class="text-center text-xs text-secondary mb-0">{{ $category->parentName}}</p>
                                        </td>

                                        <td>
                                            <p class="text-xs text-secondary mb-0">{{ $category->name }}</p>
                                        </td>


                                        <td class="align-middle text-center text-sm">
                                            <div class="container">
                                                @hasPermission('category-show')
                                                <div class="d-flex justify-content-center">
                                                    <a href="{{ route('categories.show', $category->id) }}"
                                                       class="text-xs mb-0 btn btn-success btn-sm m-1" data-toggle="tooltip"
                                                       data-original-title="Show category">
                                                        Show
                                                    </a>
                                                @else
                                                @endhasPermission
                                                @hasPermission('category-edit')
                                                        <a href="{{ route('categories.edit', $category->id) }}"
                                                       class="text-xs mb-0 btn btn-warning btn-sm m-1"
                                                       data-toggle="tooltip" data-original-title="Edit category">
                                                        Edit
                                                    </a>
                                                @else
                                                @endhasPermission
                                                @hasPermission('category-destroy')
                                                            <form method="POST"
                                                          action="{{ route('categories.destroy', $category->id) }}">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button class="text-xs mb-0 btn btn-danger btn-sm m-1"> Delete
                                                        </button>
                                                    </form>
                                                @else
                                                @endhasPermission
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <br>
                            <div class="container justify-content-center">
                                {{$categories->appends(request()->all())->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
