@extends('layouts.app')

@section('content')
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card my-4">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3 row">
                            <h4 class="text-white  ps-3 col-6">ROLE</h4>

                            <form method="GET" class="col-4">
                                <div class="input-group input-group-sm m-0 border border-white border-3">
                                    <input type="text" name="search"
                                           class="form-control ps-3 text-white"
                                           placeholder="Search...">

                                    <button class="btn btn-sm m-0 " type="submit">
                                        <div class="input-group-append opacity-10 text-white">
                                            <i style="font-size: 12px" class="fas fa-search"></i>
                                        </div>
                                    </button>
                                </div>
                            </form>

                            <div class="col-1 text-right">
{{--                                @hasPermission('role-create')--}}
                                    <a href="{{ route('roles.create') }}"
                                       class="btn btn-sm m-0 border border-white border-3 text-white">
                                        <i style="font-size: 12px" class="fas fa-plus fa-2x"></i>
                                    </a>
{{--                                @else--}}
{{--                                @endhasPermission--}}
                            </div>
                        </div>
                    </div>
                    <div class="card-body px-0 pb-2">
                        <div class="table-responsive p-0">
                            <table class="table align-items-center mb-0">
                                <thead>
                                <tr>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder">
                                        Number
                                    </th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Name</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Permission
                                    </th>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder">
                                        Action
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($roles as $role)
                                    <tr>
                                        <td>
                                            <p class="text-center text-xs text-secondary mb-0">{{ $stt = $stt + 1 }}</p>
                                        </td>

                                        <td>
                                            <p class="text-xs text-secondary mb-0">{{ $role->display_name }}</p>
                                        </td>

                                        <td>
                                            @foreach($role->permissions as $item)
                                                <p class="text-xs text-secondary mb-0">{{$item->display_name}}</p>
                                            @endforeach
                                        </td>

                                        <td class="align-middle text-center text-sm">
                                            <div class="container">
                                                <div class="d-flex justify-content-center">
                                                    @hasPermission('role-show')
                                                        <a href="{{ route('roles.show', $role->id) }}"
                                                           class="text-xs mb-0 btn btn-success btn-sm m-1" data-toggle="tooltip"
                                                           data-original-title="Show user">
                                                            Show
                                                        </a>
                                                    @else
                                                    @endhasPermission

                                                    @hasPermission('role-edit')
                                                        <a href="{{ route('roles.edit', $role->id) }}"
                                                           class="text-xs mb-0 btn btn-warning btn-sm m-1"
                                                           data-toggle="tooltip" data-original-title="Edit user">
                                                            Edit
                                                        </a>
                                                    @else
                                                    @endhasPermission

                                                    @hasPermission('role-delete')
                                                        <form method="POST"
                                                              action="{{ route('roles.destroy', $role->id) }}">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button class="text-xs mb-0 btn btn-danger btn-sm m-1"> Delete
                                                            </button>
                                                        </form>
                                                    @else
                                                    @endhasPermission

                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <br>
                            <div class="container justify-content-center">
                                {{$roles->appends(request()->all())->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
