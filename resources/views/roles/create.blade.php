@extends('layouts.app')

@section('content')
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card my-4">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3 row">
                            <h4 class="text-white  ps-3 col-6">ROLE/CREATE</h4>
                        </div>
                    </div>
                    <div class="card-body px-0 pb-2">
                        <div class="table-responsive p-0">

                            <div class="card card-plain">
                                <div class="card-body">
                                    <form role="form" method="POST" action="{{ route('roles.store') }}">
                                        @csrf
                                        <div class="input-group input-group-outline mb-3">

                                            <input type="text" class="form-control" name="name"
                                                   placeholder="Name role ...">
                                            @error('name')
                                            <span class="alert-danger" role="alert">{{$message}}</span>
                                            @enderror
                                        </div>

                                        <div class="input-group input-group-outline mb-3">
                                            <input type="text" class="form-control" name="display_name"
                                                   placeholder="Display name ...">
                                            @error('display_name')
                                            <span class="alert-danger" role="alert">{{$message}}</span>
                                            @enderror
                                        </div>


                                        <div class="d-flex">
                                            @foreach($permissions as $group => $permissionGroup)
                                                <div class="container">
                                                    <div class="row">
                                                        @foreach($permissionGroup as $permission)
                                                            <div class="container pl-0">
                                                                <div class="form-check">
                                                                    <label class="form-check-label text-dark">
                                                                        <input class="form-check-input"
                                                                               type="checkbox"
                                                                               name="permission_id[]"
                                                                               id="{{$permission->group_name}}"
                                                                               value="{{$permission->id}}">
                                                                        {{$permission->display_name}}
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>


                                        @hasPermission('role-store')
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-lg bg-gradient-primary btn-lg w-100 mt-4 mb-0">Create</button>
                                        </div>
                                        @else
                                            @endhasPermission
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
