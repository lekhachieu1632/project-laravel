@extends('layouts.app')

@section('content')
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card my-4">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3 row">
                            <h4 class="text-white  ps-3 col-6">ROLE/SHOW</h4>


                        </div>
                    </div>
                    <div class="card-body px-0 pb-2">
                        <div class="table-responsive p-0">
                            <table class="table align-items-center mb-0">
                                <thead>
                                <tr>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder">
                                        Id
                                    </th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Name</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Permission
                                    </th>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder">
                                        Action
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <p class="text-center text-xs text-secondary mb-0">{{ $role->id }}</p>
                                        </td>

                                        <td>
                                            <p class="text-xs text-secondary mb-0">{{ $role->display_name }}</p>
                                        </td>

                                        <td>
                                            @foreach($role->permissions as $item)
                                                <p class="text-xs text-secondary mb-0">{{$item->display_name}}</p>
                                            @endforeach
                                        </td>

                                        <td class="align-middle text-center text-sm">
                                            <a href=""
                                               class="text-xs mb-0 btn btn-success btn-sm" data-toggle="tooltip"
                                               data-original-title="Show user">
                                                Show
                                            </a>
                                            <a href="javascript:" class="text-xs mb-0 btn btn-warning btn-sm"
                                               data-toggle="tooltip" data-original-title="Edit user">
                                                Edit
                                            </a>
                                            <a href="javascript:" class="text-xs mb-0 btn btn-danger btn-sm"
                                               data-toggle="tooltip" data-original-title="Delete user">
                                                Delete
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
