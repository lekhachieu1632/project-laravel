<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            ['name' => 'user-view', 'display_name' => 'User View', 'group_name' => 'user'],
            ['name' => 'user-create', 'display_name' => 'User Create', 'group_name' => 'user'],
            ['name' => 'user-store', 'display_name' => 'User Store', 'group_name' => 'user'],
            ['name' => 'user-edit', 'display_name' => 'User Edit', 'group_name' => 'user'],
            ['name' => 'user-update', 'display_name' => 'User Update', 'group_name' => 'user'],
            ['name' => 'user-delete', 'display_name' => 'User Delete', 'group_name' => 'user'],
            ['name' => 'user-show', 'display_name' => 'User Show', 'group_name' => 'user'],

            ['name' => 'role-view', 'display_name' => 'Role View', 'group_name' => 'role'],
            ['name' => 'role-create', 'display_name' => 'Role Create', 'group_name' => 'role'],
            ['name' => 'role-store', 'display_name' => 'Role Store', 'group_name' => 'role'],
            ['name' => 'role-edit', 'display_name' => 'Role Edit', 'group_name' => 'role'],
            ['name' => 'role-update', 'display_name' => 'Role Update', 'group_name' => 'role'],
            ['name' => 'role-delete', 'display_name' => 'Role Delete', 'group_name' => 'role'],
            ['name' => 'role-show', 'display_name' => 'Role Show', 'group_name' => 'role'],

            ['name' => 'product-view', 'display_name' => 'Product View', 'group_name' => 'product'],
            ['name' => 'product-create', 'display_name' => 'Product Create', 'group_name' => 'product'],
            ['name' => 'product-store', 'display_name' => 'Product Store', 'group_name' => 'product'],
            ['name' => 'product-edit', 'display_name' => 'Product Edit', 'group_name' => 'product'],
            ['name' => 'product-update', 'display_name' => 'Product Update', 'group_name' => 'product'],
            ['name' => 'product-delete', 'display_name' => 'Product Delete', 'group_name' => 'product'],
            ['name' => 'product-show', 'display_name' => 'Product Show', 'group_name' => 'product'],

            ['name' => 'category-view', 'display_name' => 'Category View', 'group_name' => 'category'],
            ['name' => 'category-create', 'display_name' => 'Category Create', 'group_name' => 'category'],
            ['name' => 'category-store', 'display_name' => 'Category Store', 'group_name' => 'category'],
            ['name' => 'category-edit', 'display_name' => 'Category Edit', 'group_name' => 'category'],
            ['name' => 'category-update', 'display_name' => 'Category Update', 'group_name' => 'category'],
            ['name' => 'category-delete', 'display_name' => 'Category Delete', 'group_name' => 'category'],
            ['name' => 'category-show', 'display_name' => 'Category Show', 'group_name' => 'category']
        ]);
    }
}
