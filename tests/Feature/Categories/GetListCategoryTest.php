<?php

namespace Tests\Feature\Categories;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListCategoryTest extends TestCase
{
    public function getCategoryRoute()
    {
        return route('categories.index');
    }

    /** @test */
    public function authenticated_super_admin_can_get_all_categories()
    {
        $this->loginAsSuperAdmin();
        $response = $this->get($this->getCategoryRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.index');
    }

    /** @test */
    public function authenticated_user_can_get_list_category()
    {
        $this->loginAsUserWithPermission('category-view');
        $response = $this->get($this->getCategoryRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.index');
    }

    /** @test */
    public function unauthenticated_user_can_not_get_all_categories()
    {
        $response = $this->get($this->getCategoryRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
}
