<?php

namespace Tests\Feature\Categories;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class GetCategoryTest extends TestCase
{
    public function getShowRoute($id)
    {
        return route('categories.show', $id);
    }

    /** @test */
    public function authenticated_super_admin_can_get_category()
    {
        $this->loginAsSuperAdmin();
        $category = Category::factory()->create();
        $response = $this->get($this->getShowRoute($category->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.show');
        $response->assertSee($category->name);
    }

    /** @test */
    public function authenticated_user_can_get_category()
    {
        $this->loginAsUserWithPermission('category-show');
        $category = Category::factory()->create();
        $response = $this->get($this->getShowRoute($category->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.show');
        $response->assertSee($category->name);
    }

    /** @test */
    public function unauthenticated_user_can_not_get_category()
    {
        $id = 1;
        $response = $this->get($this->getShowRoute($id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
}
