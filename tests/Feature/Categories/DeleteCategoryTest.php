<?php

namespace Tests\Feature\Categories;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class DeleteCategoryTest extends TestCase
{
    public function getDeleteRoute($id)
    {
        return route('categories.destroy', $id);
    }

    public function getCategoryRoute()
    {
        return route('categories.index');
    }

    /** @test */
    public function authenticated_and_user_has_permission_user_can_delete_category()
    {
        $this->loginAsUserWithPermission('category-delete');
        $category = Category::factory()->create();
        $response = $this->delete($this->getDeleteRoute($category->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getCategoryRoute());
    }

    /** @test */
    public function authenticated_super_admin_can_delete_category()
    {
        $category = Category::factory()->create();
        $this->loginAsUserWithPermission('category-delete');
        $response = $this->delete($this->getDeleteRoute($category->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getCategoryRoute());
    }

    /** @test */
    public function unauthenticated_user_can_not_delete_category()
    {
        $category = Category::factory()->create();
        $response = $this->delete($this->getDeleteRoute($category->id));
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test */
    public function unauthenticated_super_admin_can_not_delete_category()
    {
        $category = Category::factory()->create();
        $response = $this->delete($this->getDeleteRoute($category->id));
        $response->assertStatus(Response::HTTP_FOUND);
    }
}
