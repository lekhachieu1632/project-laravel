<?php

namespace Tests\Feature\Categories;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class UpdateCategoryTest extends TestCase
{
    public function getCategoryRoute()
    {
        return route('categories.index');
    }

    public function getEditRoute($id)
    {
        return route('categories.edit', $id);
    }

    public function getUpdateRoute($id)
    {
        return route('categories.update', $id);
    }

    /** @test */
    public function super_admin_can_see_edit_category_form()
    {
        $this->loginAsSuperAdmin();
        $category = Category::factory()->create();
        $updateData = [
            'name' => $this->faker->unique->word(),
            'parent_id' => '1',
        ];
        $response = $this->get($this->getEditRoute($category->id), $updateData);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.edit');
        $response->assertSee(['name', 'parent_id']);
    }

    /** @test */
    public function super_admin_can_update_category()
    {
        $this->loginAsSuperAdmin();
        $category = Category::factory()->create();
        $updateData = [
            'name' => $this->faker->unique->word(),
            'parent_id' => '1',
        ];
        $response = $this->put($this->getUpdateRoute($category->id), $updateData);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('categories', $updateData);
    }

    /** @test */
    public function super_admin_can_not_update_category_if_name_is_null()
    {
        $this->loginAsSuperAdmin();
        $category = Category::factory()->create();
        $updateData = [
            'name' => null,
            'parent_id' => '1',
        ];
        $response = $this->put($this->getUpdateRoute($category->id), $updateData);
        $response->assertSessionHasErrors(['name']);
        $this->assertDatabaseMissing('categories', $updateData);
    }

    /** @test */
    public function authenticated_user_has_permission_can_see_edit_category_form()
    {
        $this->loginAsUserWithPermission('category-edit');
        $category = Category::factory()->create();
        $response = $this->get($this->getEditRoute($category->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.edit');
        $response->assertSee(['name', 'parent_id']);
    }

    /** @test */
    public function authenticated_user_has_permission_can_update_category()
    {
        $this->loginAsUserWithPermission('category-update');
        $category = Category::factory()->create();
        $updateData = [
            'name' => $this->faker->unique->word(),
            'parent_id' => '1',
        ];
        $response = $this->put($this->getUpdateRoute($category->id), $updateData);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('categories', $updateData);
    }

    /** @test */
    public function authenticated_user_has_permission_can_not_update_category_if_name_is_null()
    {
        $this->loginAsUserWithPermission('category-update');
        $category = Category::factory()->create();
        $updateData = [
            'name' => null,
            'parent_id' => '1',
        ];
        $response = $this->put($this->getUpdateRoute($category->id), $updateData);
        $response->assertSessionHasErrors(['name']);
        $this->assertDatabaseMissing('categories', $updateData);
    }

    /** @test */
    public function unauthenticated_user_can_not_see_edit_category_form()
    {
        $category = Category::factory()->create();
        $response = $this->get($this->getEditRoute($category->id));
        $response->assertStatus(Response::HTTP_FOUND);
    }
}
