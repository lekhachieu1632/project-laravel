<?php

namespace Tests\Feature\Categories;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class CreateCategoryTest extends TestCase
{
    public function getCategoryRoute()
    {
        return route('categories.index');
    }

    public function getCreateRoute()
    {
        return route('categories.create');
    }

    public function getStoreRoute()
    {
        return route('categories.store');
    }

    /** @test */
    public function authenticated_super_admin_can_create_new_category()
    {
        $this->loginAsSuperAdmin();
        $createData = [
            'name' => $this->faker->unique->word(),
            'parent_id' => 1,
        ];
        $response = $this->post($this->getStoreRoute(), $createData);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('categories', $createData);
        $response->assertRedirect($this->getCategoryRoute());
    }

    /** @test */
    public function authenticated_and_user_has_permission_user_can_create_new_category()
    {
        $this->loginAsUserWithPermission('category-store');
        $createData = [
            'name' => $this->faker->unique->word(),
            'parent_id' => 1,
        ];
        $response = $this->post($this->getStoreRoute(), $createData);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('categories', $createData);
        $response->assertRedirect($this->getCategoryRoute());
    }

    /** @test */
    public function unauthenticated_user_can_not_create_new_category()
    {
        $createData = [
            'name' => $this->faker->unique->word(),
            'parent_id' => 1,
        ];
        $response = $this->post($this->getStoreRoute(), $createData);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function user_can_not_create_new_user_if_name_field_is_null()
    {
        $this->loginAsUserWithPermission('category-store');
        $createData = [
            'name' => null,
            'parent_id' => 1,
        ];
        $response = $this->postJson($this->getStoreRoute(), $createData);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function super_admin_can_not_create_new_user_if_name_field_is_null()
    {
        $this->loginAsSuperAdmin();
        $createData = [
            'parent_id' => 1,
        ];
        $response = $this->postJson($this->getStoreRoute(), $createData);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
