<?php

namespace Tests\Feature\Product;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class UpdateProductTest extends TestCase
{
    public function getEditRoute($id)
    {
        return route('products.edit', $id);
    }

    public function getUpdateRoute($id)
    {
        return route('products.update', $id);
    }

    public function setDataUpdate($data = [])
    {
        return array_merge([
            'name' => $this->faker->unique->name,
            'price' => '2000000',
            'description' => $this->faker->text,
        ], $data);
    }

    /** @test */
    public function super_admin_can_see_edit_product_form()
    {
        $this->loginAsSuperAdmin();
        $product = Product::factory()->create();
        $response = $this->get($this->getEditRoute($product->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.edit');
        $response->assertSee(['name', 'price']);
    }
    /** @test  */
    public function authenticated_user_has_permission_can_see_edit_product_form()
    {
        $this->loginAsUserWithPermission('product-edit');
        $product = Product::factory()->create();
        $response = $this->get($this->getEditRoute($product->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.edit');
        $response->assertSee(['name', 'price']);
    }

    /** @test */
    public function authenticated_super_admin_can_update_product()
    {
        $this->loginAsSuperAdmin();
        $product = Product::factory()->create();
        $data = $this->setDataUpdate();
        $response = $this->put($this->getUpdateRoute($product->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('products', $data);
    }

    /** @test */
    public function authenticated_user_has_permission_can_update_product()
    {
        $this->loginAsUserWithPermission('product-update');
        $product = Product::factory()->create();
        $data = $this->setDataUpdate();
        $response = $this->put($this->getUpdateRoute($product->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('products', $data);
    }

    /** @test */
    public function authenticated_super_admin_can_not_update_role_if_name_is_null()
    {
        $this->loginAsSuperAdmin();
        $product = Product::factory()->create();
        $data = $this->setDataUpdate(['name' => null]);
        $response = $this->put($this->getUpdateRoute($product->id), $data);
        $response->assertSessionHasErrors(['name']);
        $this->assertDatabaseMissing('products', $data);
    }

    /** @test */
    public function authenticated_user_has_permission_can_not_update_product_if_name_is_null()
    {
        $this->loginAsUserWithPermission('product-update');
        $product = Product::factory()->create();
        $data = $this->setDataUpdate(['name' => null]);
        $response = $this->put($this->getUpdateRoute($product->id), $data);
        $response->assertSessionHasErrors(['name']);
        $this->assertDatabaseMissing('products', $data);
    }

    /** @test */
    public function unauthenticated_user_can_not_see_edit_role_form()
    {
        $product = Product::factory()->create();
        $response = $this->get($this->getEditRoute($product->id));
        $response->assertStatus(Response::HTTP_FOUND);
    }
}
