<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class DeleteProductTest extends TestCase
{
    public function getProductRoute()
    {
        return route('products.index');
    }

    public function getDeleteRoute($id)
    {
        return route('products.destroy', $id);
    }

    /** @test */
    public function authenticated_super_admin_can_delete_product()
    {
        $this->loginAsSuperAdmin();
        $product = Product::factory()->create();
        $response = $this->delete($this->getDeleteRoute($product->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getProductRoute());
    }

    /** @test */
    public function authenticated_user_has_permission_can_delete_product()
    {
        $this->loginAsUserWithPermission('product-delete');
        $product = Product::factory()->create();
        $response = $this->delete($this->getDeleteRoute($product->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getProductRoute());
    }

    /** @test */
    public function authenticated_user_without_permission_can_not_delete_product()
    {
        $this->loginAsUser();
        $product = Product::factory()->create();
        $response = $this->delete($this->getDeleteRoute($product->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $this->assertDatabaseHas('products', $product->toArray());
    }

    /** @test */
    public function unauthenticated_user_can_not_delete_product()
    {
        $product = Product::factory()->create();
        $response = $this->delete($this->getDeleteRoute($product->id));
        $this->assertDatabaseHas('products', $product->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
    }

}
