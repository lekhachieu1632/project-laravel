<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GetProductTest extends TestCase
{
    public function getRoute($id)
    {
        return route('products.show', $id);
    }

    /** @test  */
    public function authenticated_super_admin_can_get_product()
    {
        $this->loginAsSuperAdmin();
        $product = Product::factory()->create();
        $response = $this->get($this->getRoute($product->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.show');
        $response->assertSee($product->name);
    }

    /** @test  */
    public function authenticated_user_has_permission_can_get_product()
    {
        $this->loginAsUserWithPermission('product-show');
        $product = Product::factory()->create();
        $response = $this->get($this->getRoute($product->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.show');
        $response->assertSee($product->name);
    }

    /** @test  */
    public function authenticated_user_without_permission_can_not_get_product()
    {
        $this->loginAsUser();
        $product = Product::factory()->create();
        $response = $this->get($this->getRoute($product->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $response->assertDontSee($product->name);
    }

    /** @test */
    public function unauthenticated_user_can_not_get_product()
    {
        $product = Product::factory()->create();
        $response = $this->get($this->getRoute($product->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertDontSee($product->name);
    }

}
