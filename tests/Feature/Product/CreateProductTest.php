<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;
use App\Models\Category;

class CreateProductTest extends TestCase
{
    public function getCreateRoute()
    {
        return route('products.create');
    }

    public function getStoreRoute()
    {
        return route('products.store');
    }


    public function setDataCreate($data = [])
    {
        return array_merge([
            'name' => $this->faker->name,
            'price' => '10000',
            'description' => $this->faker->text,
            'category_id' => Category::factory()->create()->id,
        ], $data);
    }

    /** @test */
    public function authenticated_super_admin_can_see_create_product_form()
    {
        $this->loginAsSuperAdmin();
        $response = $this->get($this->getCreateRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.create');
        $response->assertSee(['name', 'price']);
    }

    /** @test */
    public function authenticated_user_has_permission_can_see_create_product_form()
    {
        $this->loginAsUserWithPermission('product-create');
        $response = $this->get($this->getCreateRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.create');
        $response->assertSee(['name', 'price']);
    }

    /** @test */
    public function authenticated_super_admin_can_create_new_product()
    {
        $this->loginAsSuperAdmin();
        $data = $this->setDataCreate();
        $productCountBefore = Product::count();
        $response = $this->post($this->getStoreRoute(), $data);
        $productCountAfter = Product::count();
        $this->assertEquals($productCountBefore + 1, $productCountAfter);
        $this->assertDatabaseHas('products', ['name' => $data['name']]);
        $response->assertStatus(Response::HTTP_OK);
    }

    /** @test */
    public function authenticated_user_has_permission_can_create_new_product()
    {
        $this->loginAsUserWithPermission('product-store');
        $data = $this->setDataCreate();
        $response = $this->post($this->getStoreRoute(), $data);
        $this->assertDatabaseHas('products', ['name' => $data['name']]);
        $response->assertStatus(Response::HTTP_OK);
    }

    /** @test */
    public function authenticated_super_admin_can_not_create_new_product_if_name_and_price_is_null()
    {
        $this->loginAsSuperAdmin();
        $product = Product::factory()->make(['name' => null, 'price' => null])->toArray();
        $response = $this->post($this->getStoreRoute(), $product);
        $response->assertSessionHasErrors(['name', 'price']);
    }

    /** @test */
    public function authenticated_user_has_permission_can_not_create_new_product_if_name_and_price_are_null()
    {
        $this->loginAsUserWithPermission('product-store');
        $product = Product::factory()->make(['name' => null, 'price' => null])->toArray();
        $response = $this->post($this->getStoreRoute(), $product);
        $response->assertSessionHasErrors(['name', 'price']);
    }

    /** @test */
    public function authenticates_user_without_permission_can_not_create_new_product()
    {
        $this->loginAsUser();
        $data = $this->setDataCreate();
        $response = $this->post($this->getStoreRoute(), $data);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function unauthenticated_user_can_not_see_create_product_form()
    {
        $response = $this->get($this->getCreateRoute());
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test */
    public function unauthenticated_user_can_not_create_product()
    {
        $data = $this->setDataCreate();
        $response = $this->post($this->getStoreRoute(), $data);
        $response->assertStatus(Response::HTTP_FOUND);
    }


}
