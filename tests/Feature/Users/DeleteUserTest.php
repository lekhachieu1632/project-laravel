<?php

namespace Tests\Feature\Users;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;
use App\Models\User;

class DeleteUserTest extends TestCase
{
    public function getDeleteRoute($id)
    {
        return route('users.destroy', $id);
    }

    public function getUserRoute()
    {
        return route('users.index');
    }

    /** @test */
    public function super_admin_can_delete_user()
    {
        $this->loginAsSuperAdmin();
        $user = User::factory()->create();
        $response = $this->delete($this->getDeleteRoute($user->id));
        $this->assertDatabaseMissing('users', $user->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getUserRoute());
    }

    /** @test */
    public function authenticated_user_has_permission_can_delete_user()
    {
        $this->loginAsUserWithPermission('user-delete');
        $user = User::factory()->create();
        $response = $this->delete($this->getDeleteRoute($user->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('users.index'));
    }

    /** @test */
    public function authenticated_use_without_permission_can_not_delete_user()
    {
        $this->loginAsUser();
        $user = User::factory()->create();
        $response = $this->delete($this->getDeleteRoute($user->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function unauthenticated_user_can_not_delete_user()
    {
        $user = User::factory()->create();
        $response = $this->delete($this->getDeleteRoute($user->id));
        $response->assertStatus(Response::HTTP_FOUND);
    }
}
