<?php

namespace Tests\Feature\Users;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class UpdateUserTest extends TestCase
{
    use WithFaker;
    public function getEditRoute($id)
    {
        return route('users.edit', $id);
    }

    public function getUpdateRoute($id)
    {
        return route('users.update', $id);
    }

    public function getUserRoute()
    {
        return route('users.index');
    }

    /** @test */
    public function super_admin_can_see_edit_form()
    {
        $this->loginAsSuperAdmin();
        $user = User::factory()->create();
        $response = $this->get($this->getEditRoute($user->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.edit');
        $response->assertSee($user->name)->assertSee($user->email);
    }

    /** @test */
    public function super_admin_can_update_user()
    {
        $this->loginAsSuperAdmin();
        $user = User::factory()->create();
        $data = [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail(),
        ];
        $response = $this->put($this->getUpdateRoute($user->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getUserRoute());
        $this->assertDatabaseHas('users', ['id' => $user->id]);
    }

    /** @test */
    public function super_admin_can_not_update_user_name_and_email_are_null()
    {
        $this->loginAsSuperAdmin();
        $user = User::factory()->create();
        $data = [
            'name' => null,
            'email' => null,
        ];
        $response = $this->put($this->getUpdateRoute($user->id), $data);
        $response->assertSessionHasErrors(['name', 'email']);
    }

    /** @test */
    public function authenticated_user_has_permission_can_see_edit_user_form()
    {
        $this->loginAsUserWithPermission('user-edit');
        $user = User::factory()->create();
        $response = $this->get($this->getEditRoute($user->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.edit');
        $response->assertSee('name')->assertSee('email');
    }

    /** @test */
    public function authenticated_user_has_permission_can_update_user_if_user_exist()
    {
        $this->loginAsUserWithPermission('user-update');
        $user = User::factory()->create();
        $data = [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail(),
        ];
        $response = $this->put($this->getUpdateRoute($user->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getUserRoute());
        $this->assertDatabaseHas('users', ['id' => $user->id]);
    }

    /** @test */
    public function authenticated_user_has_permission_can_not_update_email_if_name_are_null()
    {
        $this->loginAsUserWithPermission('user-edit');
        $user = User::factory()->create();
        $data = [
            'name' => null,
            'email' => null
        ];
        $response = $this->put($this->getUpdateRoute($user->id), $data);
        $this->assertDatabaseMissing('users', $data);
    }

    /** @test */
    public function unauthenticated_user_can_not_see_edit_user_form()
    {
        $user = User::factory()->create();
        $response = $this->get($this->getEditRoute($user->id));
        $response->assertStatus(Response::HTTP_FOUND);
    }
}
