<?php

namespace Tests\Feature\Users;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;
use App\Models\User;

class GetUserTest extends TestCase
{
    public function getShowRoute($id)
    {
        return route('users.show', $id);
    }

    /** @test */
    public function super_admin_get_user()
    {
        $this->loginAsSuperAdmin();
        $user = User::factory()->create();
        $response = $this->get($this->getShowRoute($user->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.show');
    }

    /** @test */
    public function authenticated_user_have_permission_can_get_user()
    {
        $this->loginAsUserWithPermission('user-show');
        $user = User::factory()->create();
        $response = $this->get($this->getShowRoute($user->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.show');
        $response->assertSee($user->name);
    }

    /** @test */
    public function unauthenticated_user_can_not_get_user()
    {
        $user = User::factory()->create();
        $response = $this->get($this->getShowRoute($user->id));
        $response->assertStatus(Response::HTTP_FOUND);
    }
}
