<?php

namespace Tests\Feature\Users;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListUserTest extends TestCase
{
    public function getUserRoute()
    {
        return route('users.index');
    }

    /** @test */
    public function super_admin_can_get_all_users()
    {
        $this->loginAsSuperAdmin();
        $user = User::factory()->make();
        $response = $this->get($this->getUserRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.index');
        $response->assertViewHas('users');
    }

    /** @test */
    public function authenticated_user_has_permission_can_get_all_users()
    {
        $this->loginAsUserWithPermission('user-view');
        $response = $this->get($this->getUserRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.index');
        $response->assertViewHas('users');
    }

    /** @test */
    public function unauthenticated_user_can_get_user()
    {
        $user = User::factory()->make();
        $response = $this->get($this->getUserRoute());
        $response->assertStatus(Response::HTTP_FOUND);
    }
}
