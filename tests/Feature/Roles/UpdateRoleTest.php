<?php

namespace Tests\Feature\Roles;

use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateRoleTest extends TestCase
{
    public function getEditRoute($id)
    {
        return route('roles.edit', $id);
    }

    public function getUpdateRoute($id)
    {
        return route('roles.update', $id);
    }

    /** @test */
    public function super_admin_can_see_edit_role_form()
    {
        $this->loginAsSuperAdmin();
        $role = Role::factory()->create();
        $response = $this->get($this->getEditRoute($role->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('roles.edit');
        $response->assertSee(['name', 'display_name']);
    }

    /** @test */
    public function super_admin_can_update_role()
    {
        $this->loginAsSuperAdmin();
        $role = Role::factory()->create();
        $data = [
            'name' => 'min',
            'display_name' => 'Hieu test',
        ];
        $response = $this->put($this->getUpdateRoute($role->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('roles', $data);
    }

    /** @test */
    public function super_admin_can_not_update_role_if_name_is_null()
    {
        $this->loginAsSuperAdmin();
        $role = Role::factory()->create();
        $data = [
            'name' => null,
            'display_name' => null,
        ];
        $response = $this->put($this->getUpdateRoute($role->id), $data);
        $response->assertSessionHasErrors(['name']);
        $this->assertDatabaseMissing('roles', $data);
    }

    /** @test */
    public function authenticated_user_has_permission_can_see_edit_form()
    {
        $this->loginAsUserWithPermission('role-edit');
        $role = Role::factory()->create();
        $response = $this->get($this->getEditRoute($role->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('roles.edit');
        $response->assertSee(['name', 'display_name']);
    }

    /** @test */
    public function authenticated_user_has_permission_can_update_role()
    {
        $this->loginAsUserWithPermission('role-update');
        $role = Role::factory()->create();
        $data = [
            'name' => 'hieu',
            'display_name' => 'Hieu test',
        ];
        $response = $this->put($this->getUpdateRoute($role->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('roles', $data);
    }

    /** @test */
    public function authenticated_user_has_permission_can_not_update_role_if_name_is_null()
    {
        $this->loginAsUserWithPermission('role-update');
        $role = Role::factory()->create();
        $data = [
            'name' => null,
            'display_name' => 'Minh test'
        ];
        $response = $this->put($this->getUpdateRoute($role->id), $data);
        $response->assertSessionHasErrors(['name']);
        $this->assertDatabaseMissing('roles', $data);
    }

    /** @test */
    public function unauthenticated_user_can_not_see_edit_role_form()
    {
        $role = Role::factory()->create();
        $response = $this->get($this->getEditRoute($role->id));
        $response->assertStatus(Response::HTTP_FOUND);
    }
}
