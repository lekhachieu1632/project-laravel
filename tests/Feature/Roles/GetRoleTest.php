<?php

namespace Tests\Feature\Roles;

use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetRoleTest extends TestCase
{
    public function getShowRoleRoute($id)
    {
        return route('roles.show', $id);
    }
    /** @test */
    public function authenticated_super_admin_can_get_role()
    {
        $this->loginAsSuperAdmin();
        $role = Role::factory()->create();
        $response = $this->get($this->getShowRoleRoute($role->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('roles.show');
        $response->assertSee($role->display_name);
    }

    /** @test */
    public function authenticated_user_have_permission_can_get_role()
    {
        $this->loginAsUserWithPermission('role-show');
        $role = Role::factory()->create();
        $response = $this->get($this->getShowRoleRoute($role->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('roles.show');
        $response->assertSee($role->display_name);
    }

    /** @test */
    public function unauthenticated_user_can_not_get_role()
    {
        $role = Role::factory()->create();
        $response = $this->get($this->getShowRoleRoute($role->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertDontSee($role->display_name);
    }

    /** @test */
    public function authenticated_super_admin_cannot_get_role_if_role_not_exist()
    {
        $this->loginAsSuperAdmin();
        $roleID = -1;
        $response = $this->get($this->getShowRoleRoute($roleID));
        $response->assertStatus(Response::HTTP_NOT_FOUND);

    }
}
