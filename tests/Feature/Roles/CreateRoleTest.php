<?php

namespace Tests\Feature\Roles;

use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateRoleTest extends TestCase
{
    public function getRoleRoute()
    {
        return route('roles.index');
    }

    public function getCreateRoute()
    {
        return route('roles.create');
    }

    public function getStoreRoute()
    {
        return route('roles.store');
    }

    /** @test */
    public function authen_super_admin_can_see_create_role_form()
    {
        $this->loginAsSuperAdmin();
        $response = $this->get($this->getCreateRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('roles.create');
        $response->assertSee(['name', 'display_name']);
    }

    /** @test */
    public function authen_super_admin_can_create_new_role()
    {
        $this->loginAsSuperAdmin();
        $role = Role::factory()->make()->toArray();
        $response = $this->post($this->getStoreRoute(), $role);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('roles', $role);
        $response->assertRedirect($this->getRoleRoute());
    }

    /** @test */
    public function authen_super_admin_can_not_create_new_role_if_name_and_are_null()
    {
        $this->loginAsSuperAdmin();
        $role = Role::factory([
            'name' => null,
            'display_name' => $this->faker->jobTitle,
        ])->make()->toArray();
        $response = $this->post($this->getStoreRoute(), $role);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticated_user_has_permission_can_see_create_role_form()
    {
        $this->loginAsUserWithPermission('role-create');
        $response = $this->get($this->getCreateRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('roles.create');
        $response->assertSee('name');
    }

    /** @test */
    public function authenticated_user_has_permission_can_create_new_role()
    {
        $this->loginAsUserWithPermission('role-store');
        $role = Role::factory()->make()->toArray();
        $response = $this->post($this->getStoreRoute(), $role);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('roles', $role);
        $response->assertRedirect($this->getRoleRoute());
    }

    /** @test */
    public function authenticated_user_has_permission_can_not_create_new_role_if_name_and_display_name_are_null()
    {
        $this->loginAsUserWithPermission('role-store');
        $role = Role::factory([
            'name' => null,
            'display_name' => null,
        ])->make()->toArray();
        $response = $this->post($this->getStoreRoute(), $role);
        $response->assertSessionHasErrors(['name', 'display_name']);
    }

    /** @test */
    public function unauthenticated_user_can_not_see_create_form()
    {
        $response = $this->get($this->getCreateRoute());
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test */
    public function unauthenticated_user_can_not_create_role()
    {
        $role = Role::factory()->make()->toArray();
        $response = $this->post($this->getStoreRoute(), $role);
        $response->assertStatus(Response::HTTP_FOUND);
    }
}
