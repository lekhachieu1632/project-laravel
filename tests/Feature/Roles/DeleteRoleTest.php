<?php

namespace Tests\Feature\Roles;

use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteRoleTest extends TestCase
{
    public function getRoleRoute()
    {
        return route('roles.index');
    }

    public function getDeleteRoute($id)
    {
        return route('roles.destroy', $id);
    }

    /** @test */
    public function authenticated_super_admin_can_delete_role()
    {
        $this->loginAsSuperAdmin();
        $role = Role::factory()->create();
        $response = $this->delete($this->getDeleteRoute($role->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRoleRoute());
    }

    /** @test */
    public function authenticated_user_has_permission_can_delete_role()
    {
        $this->loginAsUserWithPermission('role-delete');
        $role = Role::factory()->create();
        $response = $this->delete($this->getDeleteRoute($role->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRoleRoute());
    }

    /** @test */
    public function authenticated_user_without_permission_can_not_delete_role()
    {
        $this->loginAsUser();
        $role = Role::factory()->create();
        $response = $this->delete($this->getDeleteRoute($role->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $this->assertDatabaseHas('roles', $role->toArray());
    }

    /** @test */
    public function unauthenticated_user_can_not_delete_role()
    {
        $role = Role::factory()->create();
        $response = $this->delete($this->getDeleteRoute($role->id));
        $this->assertDatabaseHas('roles', $role->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
}
