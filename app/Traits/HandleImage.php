<?php

namespace App\Traits;

use Intervention\Image\Facades\Image;

trait HandleImage
{
    protected string $imageDefault = 'default-product-image.png';
    protected string $imagePath = 'images/Uploads/';

    public function verifyImage($request): bool
    {
        return $request->hasFile('image') && $request->file('image');
    }

    public function saveImage($request): string
    {
        if ($this->verifyImage($request)) {
            $file = $request->file('image');
            $fileName = time() . $file->getClientOriginalName();
            $saveLocation = $this->imagePath . $fileName;
            Image::make($file)->resize(500, 500)->save($saveLocation);
            return $fileName;
        }
        return $this->imageDefault;
    }

    public function updateImage($request, $currentImage)
    {
        if ($this->verifyImage($request)) {
            $this->deleteImage($currentImage);
            return $this->saveImage($request);
        }
        return $currentImage;
    }

    public function deleteImage($imageName)
    {
        $pathDelete = $this->imagePath . $imageName;

        if (file_exists($pathDelete) && $imageName != $this->imageDefault) {
            unlink($pathDelete);
        }
    }
}
