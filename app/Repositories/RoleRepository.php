<?php

namespace App\Repositories;

use App\Models\Role;
use App\Repositories\BaseRepository;

class RoleRepository extends BaseRepository
{
    public function model()
    {
        return Role::class;
    }

    public function search($dataSearch)
    {
        return $this->model->withName($dataSearch['search'])->latest('id')->paginate(5);
    }

    public function getWithoutSuperAdmin()
    {
        return $this->model->withoutSuperAdmin()->get();
    }

    public function findWithoutSuperAdmin($id)
    {
        return $this->model->with('permissions')->withoutSuperAdmin()->findOrFail($id);
    }
}
