<?php

namespace App\Repositories;

use App\Models\Category;
use App\Repositories\BaseRepository;

class CategoryRepository extends BaseRepository
{
    public function model()
    {
        return Category::class;
    }

    public function search($dataSearch)
    {
        return $this->model->withName($dataSearch['search'])->latest('id')->paginate(5);
    }

    public function deleteParent($id)
    {
        $category = $this->model->where('parent_id', $id);
        return $category->delete();
    }
}
