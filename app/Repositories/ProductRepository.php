<?php

namespace App\Repositories;

use App\Models\Product;
use App\Repositories\BaseRepository;

class ProductRepository extends BaseRepository
{
    public function model()
    {
        return Product::class;
    }

    public function search($dataSearch)
    {
        return $this->model->withName($dataSearch['search'])
            ->withPrice($dataSearch['search'])
            ->withCategoryName($dataSearch['category'])->latest('id')->paginate(5);
    }
}
