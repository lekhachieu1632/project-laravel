<?php

namespace App\Repositories;

use Illuminate\Contracts\Container\BindingResolutionException;


abstract class BaseRepository
{
    public $model;

    public function __construct()
    {
        $this->model = app($this->model());
    }

    abstract public function model();

    public function all()
    {
        return $this->model->all();
    }

    public function findOrFail($id, $table)
    {
        return $this->model->with($table)->findOrFail($id);
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function create($dataCreate)
    {
        return $this->model->create($dataCreate);
    }

    public function update(array $input, $id)
    {
        $model = $this->model->findOrFail($id);
        $model->fill($input);
        $model->save();

        return $model;
    }

    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    public function count()
    {
        return $this->model->all()->count();
    }
}
