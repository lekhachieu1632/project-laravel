<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\BaseRepository;

class UserRepository extends BaseRepository
{
    public function model()
    {
        return User::class;
    }

    public function search($dataSearch)
    {
        return $this->model->withName($dataSearch['search'])->withEmail($dataSearch['search'])
            ->withRoleName($dataSearch['role'])->latest('id')->paginate(5);
    }
}
