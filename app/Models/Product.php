<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\Relation;

class Product extends Model
{
    use HasFactory;

    protected $table = 'products';

    protected $fillable = [

        'name',
        'price',
        'image',
        'description',
    ];

    public function categories()
    {
        return $this->belongsToMany(
            Category::class,
            'category_product',
            'product_id',
            'category_id'
        );
    }

    public function addCategory($categoryId)
    {
        return $this->categories()->attach($categoryId);
    }

    public function syncCategory($categoryId) : array
    {
        return $this->categories()->sync($categoryId);
    }

    public function scopeWithName($query, $name)
    {
        return $query->where('name', 'Like', '%'. $name .'%');
    }

    public function scopeWithPrice($query, $price)
    {
        return $query->orWhere('price', $price);
    }

    public function scopeWithCategoryName($query, $category)
    {
        return $category ? $query->WhereHas('categories', fn ($q) => $q->where('categories.name', $category)) : null;
    }
}
