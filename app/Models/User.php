<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $table = 'users';

    protected $fillable = [
        'name',
        'email',
        'password',
        'address',
    ];

    protected $hidden = [
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsToMany(
            Role::class,
            'role_user',
            'user_id',
            'role_id',
        );
    }

    public function scopeWithName($query, $name)
    {
        return $name ? $query->where('name', 'like', '%' . $name . '%') : null;
    }

    public function scopeWithEmail($query, $email)
    {
        return $email ? $query->orWhere('email', 'like', '%' . $email . '%') : null;
    }

    public function scopeWithRoleName($query, $name)
    {
        return $name ? $query->whereHas('roles', function ($query) use ($name) {
            $query->where('name', 'like', '%' . $name . '%');
        }) : null;
    }

    public function addRole($roleId)
    {
        return $this->roles()->attach($roleId);
    }

    public function syncRole($roleId): array
    {
        return $this->roles()->sync($roleId);
    }

    public function hasPermission($permission)
    {
        foreach ($this->roles as $role){
            if($role->hasPermission($permission)){
                return true;
            }
        }
        return false;
    }

    public function hasRole($role)
    {
        return $this->roles()->where('name', $role)->exists();
    }

    public function isSuperAdmin()
    {
        return $this->hasRole('super-admin');
    }
}
