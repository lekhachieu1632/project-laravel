<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $table = 'categories';

    protected $fillable = [
        'name',
        'parent_id'
    ];

    public function products()
    {
        return $this->belongsToMany(
            Product::class,
            'category_product',
            'category_id',
            'product_id',
        );
    }

    public function parentCategory()
    {
        return $this->belongsTo(Category::class, 'parent_id', 'id');
    }

    public function scopeWithName($query, $name)
    {
        return $query->where('name', 'Like', '%' . $name . '%');
    }

    public function getParentNameAttribute()
    {
        return $this->parentCategory ? $this->parentCategory->name : null;
    }
}
