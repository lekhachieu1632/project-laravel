<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(
            [
                'roles.index',
                'roles.create',
                'roles.edit',
                'roles.show',
            ],
            'App\Http\ViewComposers\PermissionComposer',


        );

        view()->composer(
            [
                'users.index',
                'users.create',
                'users.edit',
                'users.show',
            ],
            'App\Http\ViewComposers\RoleComposer'
        );

        view()->composer(
            [
//                'categories.index',
//                'categories.create',
//                'categories.edit',
//                'categories.show',

                'products.index',
                'products.create',
                'products.edit',
                'products.show'
            ],
            'App\Http\ViewComposers\CategoryComposer'
        );

    }
}
