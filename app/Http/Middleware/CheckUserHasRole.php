<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class CheckUserHasRole
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return Response|RedirectResponse
     */
    public function handle(Request $request, Closure $next, ...$roles)
    {
        if (auth()->user()->isSuperAdmin()) {
            return $next($request);
        }

        if (is_array($roles)) {
            foreach ($roles as $role) {
                if (auth()->user()->hasRole(explode(",", $role))) {
                    return $next($request);
                }
            }
        } else {
            if (auth()->user()->hasRole($roles)) {
                return $next($request);
            }
        }

        return abort(403);
    }
}
