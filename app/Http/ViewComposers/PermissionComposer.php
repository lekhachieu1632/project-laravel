<?php

namespace App\Http\ViewComposers;

use App\Services\PermissionService;
use Illuminate\View\View;

class PermissionComposer
{
    public $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }
    public function compose(View $view)
    {
        $view->with('permissions', $this->permissionService->getWithGroupName('group_name'));
    }
}
