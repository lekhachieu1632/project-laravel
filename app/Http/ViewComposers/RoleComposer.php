<?php

namespace App\Http\ViewComposers;

use App\Services\RoleService;
use Illuminate\View\View;

class RoleComposer
{
    public $roleService;

    public function __construct(RoleService $roleService)
    {
        $this->roleService = $roleService;
    }
    public function compose(View $view)
    {
        $view->with('rolesWithoutSuperAdmin', $this->roleService->withoutSuperAdmin());
    }
}
