<?php

namespace App\Http\Controllers;

use App\Http\Requests\Category\StoreCategoryRequest;
use App\Http\Requests\Category\UpdateCategoryRequest;
use App\Http\Resources\Category\CategoryResource;
use App\Services\CategoryService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\View;

class CategoryController extends Controller
{
    protected CategoryService $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;

        View::share('categories', $this->categoryService->all());
    }

    public function index(Request $request)
    {
        $categories = $this->categoryService->search($request);
        return view('categories.index', compact('categories'));
    }

    public function create()
    {
        return view('categories.create');
    }

    public function store(StoreCategoryRequest $request)
    {
        $this->categoryService->create($request);
        return redirect()->route('categories.index');
    }

    public function show(int $id)
    {
        $category = $this->categoryService->find($id);
        return view('categories.show', compact('category'));
    }

    public function edit(int $id)
    {
        $category = $this->categoryService->find($id);
        return view('categories.edit', compact('category'));
    }

    public function update(UpdateCategoryRequest $request, int $id)
    {
        $this->categoryService->update($request, $id);
        return redirect()->route('categories.index');
    }

    public function destroy(int $id)
    {
        $this->categoryService->delete($id);
        $this->categoryService->deleteParent($id);
        return redirect()->route('categories.index');
    }
}
