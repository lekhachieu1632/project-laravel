<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\StoreUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Services\RoleService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class UserController extends Controller
{
    protected UserService $userService;
    protected RoleService $roleService;

    public function __construct(UserService $userService, RoleService $roleService)
    {
        $this->userService = $userService;
        $this->roleService = $roleService;

//        View::share('rolesWithoutSuperAdmin', $this->roleService->withoutSuperAdmin());
    }

    public function index(Request $request)
    {
        $users = $this->userService->search($request);
        return view('users.index', compact('users'));
    }

    public function create()
    {
        return view('users.create');
    }

    public function store(StoreUserRequest $request)
    {
        $this->userService->create($request);
        return redirect()->route('users.index');
    }

    public function show($id)
    {
        $user = $this->userService->find($id);
        return view('users.show', compact('user'));
    }

//    public function edit($id)
//    {
//        $user = $this->userService->findById($id);
//        $roleIds = $this->userService->getRoleId($id);
//        return view('users.edit', compact('user', 'roleIds'));
//    }

    public function edit($id)
    {
        $user = $this->userService->find($id);
        return view('users.edit', compact('user'));
    }

    public function update(UpdateUserRequest $request, int $id)
    {
        $this->userService->update($request, $id);
        return redirect()->route('users.index');
    }

    public function destroy($id)
    {
        $this->userService->delete($id);
        return redirect()->route('users.index');
    }
}
