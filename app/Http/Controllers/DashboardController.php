<?php

namespace App\Http\Controllers;

use App\Services\CategoryService;
use App\Services\ProductService;
use App\Services\RoleService;
use App\Services\UserService;

class DashboardController extends Controller
{
    protected RoleService $roleService;
    protected UserService $userService;
    protected ProductService $productService;
    protected CategoryService $categoryService;

    public function __construct(
        RoleService     $roleService,
        UserService     $userService,
        ProductService  $productService,
        CategoryService $categoryService
    ) {
        $this->middleware('auth');

        $this->roleService = $roleService;
        $this->userService = $userService;
        $this->productService = $productService;
        $this->categoryService = $categoryService;
    }

    public function index()
    {
        $countRole = $this->roleService->count();
        $countUser = $this->userService->count();
        $countProduct = $this->productService->count();
        $countCategory = $this->categoryService->count();
        return view('dashboard', compact('countRole', 'countUser', 'countCategory', 'countProduct'));
    }
}
