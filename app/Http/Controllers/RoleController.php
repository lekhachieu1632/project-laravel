<?php

namespace App\Http\Controllers;

use App\Http\Requests\Role\StoreRoleRequest;
use App\Http\Requests\Role\UpdateRoleRequest;
use App\Services\PermissionService;
use App\Services\RoleService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class RoleController extends Controller
{
    protected RoleService $roleService;
    protected PermissionService $permissionService;

    public function __construct(RoleService $roleService, PermissionService $permissionService)
    {
        $this->roleService = $roleService;
        $this->permissionService = $permissionService;
    }

    public function index(Request $request)
    {
        $roles = $this->roleService->search($request);
        $stt = 0;
        return view('roles.index', compact(['roles', 'stt']));
    }

    public function create()
    {
        return view('roles.create');
    }

    public function store(StoreRoleRequest $request)
    {
        $this->roleService->create($request);
        return redirect()->route('roles.index');
    }

    public function show(int $id)
    {
        $role = $this->roleService->find($id);
        return view('roles.show', compact('role'));
    }

    public function edit($id)
    {
        $role = $this->roleService->find($id);
        return view('roles.edit', compact('role'));
    }

    public function update(UpdateRoleRequest $request, int $id)
    {
        $this->roleService->update($request, $id);
        return redirect()->route('roles.index');
    }

    public function destroy(int $id)
    {
        $this->roleService->delete($id);
        return redirect()->route('roles.index');
    }
}
