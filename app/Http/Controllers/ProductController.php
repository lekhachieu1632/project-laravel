<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\StoreProductRequest;
use App\Http\Requests\Product\UpdateProductRequest;
use App\Services\CategoryService;
use App\Services\ProductService;
use http\Env\Response;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class ProductController extends Controller
{
    protected ProductService $productService;
    protected CategoryService $categoryService;

    public function __construct(ProductService $productService, CategoryService $categoryService)
    {
        $this->productService = $productService;
        $this->categoryService = $categoryService;
    }

    public function index(Request $request)
    {
        $products = $this->productService->search($request);
        return view('products.index', compact('products'));
    }

    public function create()
    {
        return view('products.create');
    }

    public function store(StoreProductRequest $request)
    {
        $this->productService->create($request);
        return response()->json(['message'=>'Success'], \Illuminate\Http\Response::HTTP_OK);
    }

    public function show($id)
    {
        $product = $this->productService->findById($id);
        return view('products.show', compact('product'));
    }

    public function edit(int $id)
    {
        $product = $this->productService->findById($id);
        return view('products.edit', compact('product'));
    }

    public function update(UpdateProductRequest $request, int $id)
    {
        $this->productService->update($request, $id);
        return redirect()->route('products.index');
    }

    public function destroy(int $id)
    {
        $this->productService->delete($id);
        return redirect()->route('products.index');
    }
}
