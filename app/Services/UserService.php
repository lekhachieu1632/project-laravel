<?php

namespace App\Services;

use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Hash;

class UserService
{
    protected UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function search($request)
    {
        $dataSearch = $request->all();
        $dataSearch['search'] = $request->search;
        $dataSearch['role'] = $request->role;
        return $this->userRepository->search($dataSearch);
    }

    public function all()
    {
        return $this->userRepository->all();
    }

    public function create($request)
    {
        $dataCreate = $request->all();
        $dataCreate['password'] = Hash::make($request->password);
        $user = $this->userRepository->create($dataCreate);
        $user->addRole($request->role_id);

        return $user;
    }

    public function update($request, $id)
    {
        $dataUpdate = $request->all();
        $user = $this->userRepository->update($dataUpdate, $id);
        $user->syncRole($request->role_id);

        return $user;
    }

    public function find($id)
    {
        return $this->userRepository->find($id);
    }

    public function delete($id)
    {
        return $this->userRepository->delete($id);
    }

    public function count()
    {
        return $this->userRepository->count();
    }
}
