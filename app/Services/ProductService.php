<?php

namespace App\Services;

use App\Repositories\ProductRepository;
use App\Traits\HandleImage;

class ProductService
{
    use HandleImage;

    protected ProductRepository $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function search($request)
    {
        $dataSearch = $request->all();
        $dataSearch['search'] = $request->search;
        $dataSearch['category'] = $request->category;
        return $this->productRepository->search($dataSearch);
    }

    public function all()
    {
        return $this->productRepository->all();
    }

    public function create($request)
    {
        $dataCreate = $request->all();
        $dataCreate['image'] = $this->saveImage($request);
        $product = $this->productRepository->create($dataCreate);
        $product->addCategory($request->category_id);

        return $product;
    }

    public function update($request, $id)
    {
        $dataUpdate = $request->all();
        $product = $this->productRepository->find($id);
        $dataUpdate['image'] = $this->updateImage($request, $product->image);
        $product->update($dataUpdate);
        $product->syncCategory($request->category_id);

        return $product;
    }

    public function findById($id)
    {
        return $this->productRepository->find($id);
    }

    public function delete($id)
    {
        $product = $this->productRepository->find($id);
        $product->delete();
        $this->deleteImage($product->image);
        return $product;
    }

    public function count()
    {
        return $this->productRepository->count();
    }
}
