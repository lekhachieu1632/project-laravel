<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');


Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard',[DashboardController::class, 'index'])
    ->name('dashboard');

    Route::name('roles.')->prefix('roles')->group(function (){
        Route::get('/index', [RoleController::class, 'index'])
            ->name('index')
            ->middleware('permission:role-view');

        Route::group(['middleware' => ['role:user,category', 'permission:role-create']], function() {
                Route::get('/create', [RoleController::class, 'create'])
                    ->name('create');
        });

        Route::post('/', [RoleController::class, 'store'])
            ->name('store')
            ->middleware('permission:role-store');

        Route::put('/{id}', [RoleController::class, 'update'])
            ->name('update')
            ->middleware('permission:role-update');

        Route::get('/{id}/edit', [RoleController::class, 'edit'])
            ->name('edit')
            ->middleware('permission:role-edit');

        Route::delete('/{id}', [RoleController::class, 'destroy'])
            ->name('destroy')
            ->middleware('permission:role-delete');

        Route::get('/{id}', [RoleController::class, 'show'])
            ->name('show')
            ->middleware('permission:role-show');

    });

    Route::name('users.')->prefix('users')->group(function (){
        Route::get('/index', [UserController::class, 'index'])
            ->name('index')
            ->middleware('permission:user-view');

        Route::get('/create', [UserController::class, 'create'])
            ->name('create')
            ->middleware('permission:user-create');

        Route::post('/', [UserController::class, 'store'])
            ->name('store')
            ->middleware('permission:user-store');

        Route::get('/{id}', [UserController::class, 'show'])
            ->name('show')
            ->middleware('permission:user-show');

        Route::put('/{id}', [UserController::class, 'update'])
            ->name('update')
            ->middleware('permission:user-update');

        Route::get('/{id}/edit', [UserController::class, 'edit'])
            ->name('edit')
            ->middleware('permission:user-edit');

        Route::delete('/{id}', [UserController::class, 'destroy'])
            ->name('destroy')
            ->middleware('permission:user-delete');
    });

    Route::name('categories.')->prefix('categories')->group(function (){
        Route::get('/index', [CategoryController::class, 'index'])
            ->name('index')
            ->middleware('permission:category-view');

        Route::get('/create', [CategoryController::class, 'create'])
            ->name('create')
            ->middleware('permission:category-create');

        Route::post('/', [CategoryController::class, 'store'])
            ->name('store')
            ->middleware('permission:category-store');

        Route::get('/{id}', [CategoryController::class, 'show'])
            ->name('show')
            ->middleware('permission:category-show');

        Route::put('/{id}', [CategoryController::class, 'update'])
            ->name('update')
            ->middleware('permission:category-update');

        Route::get('/{id}/edit', [CategoryController::class, 'edit'])
            ->name('edit')
            ->middleware('permission:category-edit');

        Route::delete('/{id}', [CategoryController::class, 'destroy'])
            ->name('destroy')
            ->middleware('permission:category-delete');
    });

    Route::name('products.')->prefix('products')->group(function () {
        Route::get('/index', [ProductController::class, 'index'])
            ->name('index')
            ->middleware('permission:product-view');

        Route::get('/create', [ProductController::class, 'create'])
            ->name('create')
            ->middleware('permission:product-create');

        Route::post('/', [ProductController::class, 'store'])
            ->name('store')
            ->middleware('permission:product-store');

        Route::get('/{id}', [ProductController::class, 'show'])
            ->name('show')
            ->middleware('permission:product-show');

        Route::get('/{id}/edit', [ProductController::class, 'edit'])
            ->name('edit')
            ->middleware('permission:product-edit');

        Route::put('/{id}', [ProductController::class, 'update'])
            ->name('update')
            ->middleware('permission:product-update');

        Route::delete('/{id}', [ProductController::class, 'destroy'])
            ->name('destroy')
            ->middleware('permission:product-delete');
    });
});



